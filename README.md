# MESH ASSIGNMENT BY SAMUEL AZCONA

this is a kotlin command cli app to read a csv file that contains information about services: name, category, and the dateTime where this service is available
to creating a simple recommendation engine to help the customer look up whether a home improvement service is available at a specific day and time.

## SOLUTION
read the csv is easy as we know that a csv is a comma separated file, so is just to read as string and then split every line and separate the elements.

the "hard part" is the dates and times, so I came up with two solutions:

### solution using regex [RegexExtractor.kt]
As we know the csv come with a specific format startDay-endDay;startTime-closeTIme and with ‘/’ that separates the open days and times. Within a day/time entry, there are 3 values separated by ‘;’
and also we know that the days can come in shortnames unique or the start and end separated with dash - example: mon-fri...
so we have a common pattern here then we can easily create a regex for this that identify the days in the string.

for the time is the same case we know that the days can come in 12h short format with or without minutes, and the start time and end time are separated by  ; 
so also here we have a common pattern that can be easily extracted from the string with a regex

### solution using position [PositionExtractor.kt]
In this case as we know what is the specific format of the csv file, and the positions of every element we can found the days and times base on the position.

for the days if we look at the dateTime String "Mon-Thu;11:30am;9pm / Sun;11:30am;9pm / Fri-Sat;11:30am;9:30pm" we can identify that:
the different periods are separated by / and then every period contain 3 elements the days,startTime,EndTime separated by ; so if we split by ; then we are going to have a list of 3 elements, and we know that the first element of this are always the days
so after split is easy to just extract the first element of the days and then split again by - to get the startDay and endDay separately

for the time is the same case, but these are the last two elements of the array, then we can just extract the start and end splitting by ;

### common
when we have the days extracted we have the start and the end days, so we need to create all the others days in the middle between the startDay and the endDay
to do this the easy way is just for each where the initial value is the startDay number example monday 1 to the endDay example friday 5

so we have something like
```
                for (i in it.first.value..it.second.value) {
                        add(DayOfWeek.of(i))
                    }
```
so we are going to add tue, wed, thu, and finally we are going to have monday,tue,wed,thu,friday

for the times as we have the same pattern startTime - endTime is like that we have a range so easily we can create a range of localTime start to end that contains all the possible options between the startTime and the endTIme, and we just parse the strings 9am,2:30pm, etc to localTime objects


### What is better?
we need to analyze if the csv is going to be always in this format or not, because using the position approach we have the risk that if the csv changes our code is broken.

in the other side using the pattern approach we have the risk that if the format of the days and times change or code is broken.

In my personal opinion I prefer the pattern approach because to scale this code to new formats is easy just add more regex pattern or change it to support new formats, and we don't need to touch the logic at all.
Different is the position approach that we should need to change our algorithm

## WHAT CAN BE IMPROVED:
- reduce the quantity of for loops with better abstraction
- more tests
- more validations in the csv format

with more time I would prefer to create an abstraction where you can model different things to search in string using class patterns to extract date from this string,
for example you can create a different time regex to match more possible formats variations of the times and days this would make the recommendationService algorithm less statically and more dynamic
without the need to refactor your algorithm if the csv changes. so you can just have different patterns types, time, days, category, names, to individual extract information from a string,and then you can use it whatever you want



# how run the app
***
you have an EXAMPLE csv service_hours.csv provided in this root folder, the list provided by mesh has a typo in the row 7

Mon-Thu, Sun;11:30am;9:30pm / Fri-Sat;11:30am;10pm

if you See the first Mon-Thu is separated with , instead of ; and also is missing the time information.

It should be for example:

Mon-Thu;11:30am;9pm / Sun;11:30am;9:30pm / Fri-Sat;11:30am;10pm

so this project provide a fixed csv
***
#Requirements:
- java jdk 11
- gradle > 6.8 (gradle 7 would be nice)
- if you want to use the docker-compose file you should have docker installed

## How to use it:
you can run this app in terminal using gradle ``gradle run --args="args"`` and provide the needed values for the app
````
Usage: MeshTest options_list
Options: 
    --input, -i -> full path to the input csv file, example: /home/samuel/file.csv (always required) { String }
    --withHeader, -wh [false] -> set true if your csv contains headers by default is false, in the example provide the csv dont have headers 
    --dateTime, -d -> DateTime in format yyyy-MM-dd-HH:mm **PLEASE CHECK THAT THERE IS NOT SPACE FOR THE HH:MM(always required) { String } 
    --categories, -c -> Categories string comma separated inside single quotes of the service that you want to search example: 'Construction,electrical' THIS IS OPTIONAL *** if not set will not filter by category{ String }
    --strategy, -s [REGEX] -> Extraction strategy for dateTime you can use REGEX or POSITION, regex by default { Value should be one of [regex, position] }
    --help, -h -> Usage info 
````

in your terminal you need to go to the root folder of this project then you can execute gradle run by:

# example 1:
we want to search all the service that are available today sunday at 21:35 with service type Construction, so we send the next parameters:

````
-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv
-d 2021-05-02-21:35
-c Construction
````

``
gradle run --args="-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv -d 2021-05-02-21:35 -c Construction"
``

### expected result 
````
List of business open for today SUNDAY at 21:35 for the category Construction
------------------------------------------------------------------------------------------------------------------
| name                           | category                       | day             | open            | close           |
------------------------------------------------------------------------------------------------------------------
| Helena Hot Concrete            | Construction                   | SUNDAY          | 11:30           | 23:00           |

| Quans Concrete                 | Construction                   | SUNDAY          | 12:00           | 22:00           |

------------------------------------------------------------------------------------------------------------------
````


# example 2
we want to search all the service that are available today sunday at 21:35 with service type Construction and Appliance Repairs, so we send the next parameters:

````
-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv
-d 2021-05-02-21:35
-c 'Construction,Appliance Repairs'
````

``
gradle run --args="-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv -d 2021-05-02-21:35 -c 'Construction,Appliance Repairs'"
``
### expected result
````
List of business open for today SUNDAY at 21:20 for the category Construction, Appliance Repairs
------------------------------------------------------------------------------------------------------------------
| name                           | category                                                     | day             | open            | close           |
------------------------------------------------------------------------------------------------------------------
| Helena Hot Concrete            | [Construction]                                               | SUNDAY          | 11:30           | 23:00           |

| Jack's Hammer                  | [Construction, Plumbing]                                     | SUNDAY          | 10:00           | 21:30           |

| King Kong Konstruction         | [Construction]                                               | SUNDAY          | 09:30           | 21:30           |

| Lennys Lettering               | [Construction, Appliance Repairs]                            | SUNDAY          | 11:30           | 22:30           |

| Quans Concrete                 | [Construction]                                               | SUNDAY          | 12:00           | 22:00           |

------------------------------------------------------------------------------------------------------------------
````


# example 3
we want to search all the service that are available today sunday at 21:35 without filter by service type, so we send the next parameters:

````
-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv
-d 2021-05-02-21:35
````

``
gradle run --args="-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv -d 2021-05-02-21:35"
``
### expected result 
````
List of business open for today SUNDAY at 21:35
------------------------------------------------------------------------------------------------------------------
| name                           | category                       | day             | open            | close           |
------------------------------------------------------------------------------------------------------------------
| Carl's Cats                    | Animal Homes                   | SUNDAY          | 11:30           | 22:00           |

| Danny's Depot                  | Appliance Repairs              | SUNDAY          | 11:00           | 22:00           |

| Edgars Eggs                    | Appliance Repairs              | SUNDAY          | 10:00           | 23:00           |

| Francis classic cleaning       | Carpet Cleaning/Interior       | SUNDAY          | 17:30           | 22:00           |

| Helena Hot Concrete            | Construction                   | SUNDAY          | 11:30           | 23:00           |

| Icicle breakers                | Appliance Repairs/Exterior     | SUNDAY          | 11:00           | 23:00           |

| Lennys Lettering               | Construction/Appliance Repairs | SUNDAY          | 11:30           | 22:30           |

| Nicks Nextdoor Landscaping     | Landscaping                    | SUNDAY          | 15:00           | 23:30           |

| Picture Perfect                | Painting/Interior              | SUNDAY          | 11:00           | 23:00           |

| Quans Concrete                 | Construction                   | SUNDAY          | 12:00           | 22:00           |

| Rock Lee's Rock Removal        | Landscaping/Exterior           | SUNDAY          | 11:00           | 22:00           |

| Zoos at home                   | Animal Homes                   | SUNDAY          | 11:30           | 22:00           |

------------------------------------------------------------------------------------------------------------------
````

# example 4
we want to search all the service that are available today sunday at 21:35 without filter by service type and using the position strategy to found the date times:

````
-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv
-d 2021-05-02-21:35
-s position
````

``
gradle run --args="-i /home/samuaz/IdeaProjects/meshTest/service_hours.csv -d 2021-05-02-21:35 -s position"
``
### expected result: the result should be the same as regex strategy
````
This are the list of business open for today SUNDAY at 21:35
------------------------------------------------------------------------------------------------------------------
| name                           | category                       | day             | open            | close           |
------------------------------------------------------------------------------------------------------------------
| Carl's Cats                    | Animal Homes                   | SUNDAY          | 11:30           | 22:00           |

| Danny's Depot                  | Appliance Repairs              | SUNDAY          | 11:00           | 22:00           |

| Edgars Eggs                    | Appliance Repairs              | SUNDAY          | 10:00           | 23:00           |

| Francis classic cleaning       | Carpet Cleaning/Interior       | SUNDAY          | 17:30           | 22:00           |

| Helena Hot Concrete            | Construction                   | SUNDAY          | 11:30           | 23:00           |

| Icicle breakers                | Appliance Repairs/Exterior     | SUNDAY          | 11:00           | 23:00           |

| Lennys Lettering               | Construction/Appliance Repairs | SUNDAY          | 11:30           | 22:30           |

| Nicks Nextdoor Landscaping     | Landscaping                    | SUNDAY          | 15:00           | 23:30           |

| Picture Perfect                | Painting/Interior              | SUNDAY          | 11:00           | 23:00           |

| Quans Concrete                 | Construction                   | SUNDAY          | 12:00           | 22:00           |

| Rock Lee's Rock Removal        | Landscaping/Exterior           | SUNDAY          | 11:00           | 22:00           |

| Zoos at home                   | Animal Homes                   | SUNDAY          | 11:30           | 22:00           |

------------------------------------------------------------------------------------------------------------------

````
