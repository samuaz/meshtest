package util

typealias Header = String
data class Row(val value: Map<Header, String>) : Map<Header, String> by value

fun parseCsv(content: String): List<Row> {

    // if the content is empty nothing to do here
    if (validateContent(content)) return emptyList()

    // lets clean the format and then we now that the csv use line separator so lets split every line in an array element
    val contentList = generateRows(content)
    // we know that in a csv the first line are the headers so lets take it and also clean the format
    val headers = contentList.first().split(",").filter { it != "" }
    // the columns of the csv are all the lines after the headers
    val columns = contentList.drop(1)

    return createRows(columns, headers)
}

fun parseCsvWithoutHeaders(content: String): List<Row> {

    // if the content is empty nothing to do here
    if (validateContent(content)) return emptyList()

    // lets clean the format and then we now that the csv use line separator so lets split every line in an array element
    val columns = generateRows(content)

    // as the csv don't contains headers lets add some useful columns
    val headers = listOf("name", "category", "openCloseTime")

    return createRows(columns, headers)
}

private fun generateRows(content: String) =
    content.trimIndent().replace("^\"|\"$", "").split(System.lineSeparator()).filter { it != "" }

private fun validateContent(content: String): Boolean {
    if (content.isEmpty()) {
        return true
    }
    return false
}

private fun createRows(
    seq: List<String>,
    headers: List<String>
): List<Row> {
    return seq.map { row ->
        Row(
            // we know that in a csv the elements are separated by , so we can split by , and then link every element with the header by the index
            row.split(",").filter { it != "" }.mapIndexed { i, it ->
                headers[i] to it.replace("\"", "")
            }.toMap()
        )
    }
}
