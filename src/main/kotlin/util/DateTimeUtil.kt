package util

import java.time.DayOfWeek
import java.time.LocalTime
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

object DateTimeUtil {

    fun formatTime(time: String): LocalTime {
        // the time can come with just the hour like 9am or also with minutes 9:30am so we need an special patter
        return LocalTime.parse(
            time.replace("\\s".toRegex(), ""), DateTimeFormatterBuilder()
                .appendPattern("h[:mm]")
                .appendText(ChronoField.AMPM_OF_DAY, mapOf(Pair(0L, "am"), Pair(1L, "pm")))
                .toFormatter()
        )
    }

    // util function to parse the short weekdays to DayOfWeeks
    fun dayOfWeek(day: String): DayOfWeek {
        return when (day.replace("\\s".toRegex(), "").toLowerCase()) {
            "mon" -> DayOfWeek.MONDAY
            "tue" -> DayOfWeek.TUESDAY
            "wed" -> DayOfWeek.WEDNESDAY
            "thu" -> DayOfWeek.THURSDAY
            "fri" -> DayOfWeek.FRIDAY
            "sat" -> DayOfWeek.SATURDAY
            "sun" -> DayOfWeek.SUNDAY
            else -> throw IllegalArgumentException()
        }
    }

    fun createDays(days: List<DayOfWeek>): List<DayOfWeek> {
        // if is only one day just return it we don't need to create extra dates
        return if (days.size == 1) {
            days
        } else {
            // if there is a day period from - to like mon-fri we need to create all the days between the start and end days
            days.zipWithNext().map {
                return mutableListOf<DayOfWeek>().apply {
                    // so lets foreach from the start day to the end day and create the middle days
                    for (i in it.first.value..it.second.value) {
                        add(DayOfWeek.of(i))
                    }
                }
            }
        }
    }
}