import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.cli.required
import util.parseCsv
import util.parseCsvWithoutHeaders
import java.io.File
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter


// util classes
data class CsvRow(val name: String, val category: String, val openCloseTime: String)
data class Store(val name: String, val categories: List<String>, val dayTimes: List<DayTime>)
data class DayTime(val dayOfWeek: DayOfWeek, val times: ClosedRange<LocalTime>)
data class StoreTodaySchedule(
    val name: String,
    val categories: List<String>,
    val day: DayOfWeek,
    val times: ClosedRange<LocalTime>
)
enum class ExtractionStrategy {
    REGEX,
    POSITION
}

const val dateTimeFormatValue = "yyyy-MM-dd-HH:mm"
val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeFormatValue)

// end util classes
fun main(args: Array<String>) {

    // create cli options
    val parser = ArgParser("MeshTest")
    val input by parser.option(ArgType.String, shortName = "i", description = "full path to the input csv file, example: /home/samuel/file.csv").required()
    val withHeader by parser.option(ArgType.Boolean, shortName = "wh", description = "set true if your csv contains headers by default is false, in the example provide the csv dont have headers").default(false)
    val dateTime by parser.option(
        ArgType.String,
        shortName = "d",
        description = "DateTime in format $dateTimeFormatValue"
    ).required()
    val categories by parser.option(ArgType.String, shortName = "c", description = "Categories string comma separated of the service that you want to search example: Construction,electrical")
    val strategy by parser.option(ArgType.Choice<ExtractionStrategy>(), shortName = "s", description = "Extraction strategy for dateTime you can use REGEX or POSITION, regex by default").default(ExtractionStrategy.REGEX)

    // parse args
    parser.parse(args)

    // read the needed data from the cli options
    val inputData = FileParser.readCsv(input, withHeader)
    val currentTime = LocalDateTime.parse(dateTime.replace("\\s".toRegex(), ""), dateTimeFormatter)

    // create the service base on the extractorType REGEX OR POSITION
    val recommendationService = RecommendationService(createExtractor(strategy))

    // if category is not present this mean that we don't want to filter by category
    if (categories != null) {
        println("List of business open for today ${currentTime.dayOfWeek} at ${currentTime.toLocalTime()} for the category $categories")
        printResults(recommendationService.findAvailableServiceByCategories(inputData, currentTime, categories!!.split(",")))
    } else {
        println("List of business open for today ${currentTime.dayOfWeek} at ${currentTime.toLocalTime()}")
        printResults(recommendationService.findAvailableService(inputData, currentTime))
    }
}


// file parser util
object FileParser {
     fun readCsv(filePath: String, withHeaders: Boolean): List<CsvRow> {
         return when(withHeaders) {
             true -> parseCsv(readFile(filePath)).map {
                 CsvRow(name = it["name"]!!, category = it["category"]!!, it["openCloseTime"]!!)
             }
             false -> parseCsvWithoutHeaders(readFile(filePath)).map {
                 CsvRow(name = it["name"]!!, category = it["category"]!!, it["openCloseTime"]!!)
             }
         }
    }

    private fun readFile(fileName: String) = File(fileName).inputStream().readBytes().toString(Charsets.UTF_8)
}


private fun createExtractor(approach: ExtractionStrategy): Extractor {
   return when(approach) {
        ExtractionStrategy.REGEX -> RegexExtractor
        ExtractionStrategy.POSITION -> PositionExtractor
    }
}
// end file parser util


// print a table with results
private fun printResults(stores: List<StoreTodaySchedule>){
    val leftAlignFormat = "| %-30s | %-60s | %-15s | %-15s | %-15s |%n"
    System.out.format("------------------------------------------------------------------------------------------------------------------%n")
    System.out.format(leftAlignFormat, "name", "category", "day", "open", "close")
    System.out.format("------------------------------------------------------------------------------------------------------------------%n")
    stores.forEach {
        System.out.format(leftAlignFormat, it.name, it.categories, it.day, it.times.start, it.times.endInclusive)
        println()
    }
    System.out.format("------------------------------------------------------------------------------------------------------------------%n")
}




