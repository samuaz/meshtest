import util.DateTimeUtil
import util.DateTimeUtil.dayOfWeek
import java.time.DayOfWeek
import java.time.LocalTime

object PositionExtractor: Extractor {

    override fun extractTimes(value: String): List<LocalTime> {
        // we know that the time from - to is separated by ; example 11:30am;9pm so lets extract it and format
        return value.split(";").drop(1).map { time ->
            DateTimeUtil.formatTime(time)
        }
    }

    override fun extractDays(value: String): List<DayOfWeek> {
        // we no that every dates time period is separated by ; example Mon-Thu;11:30am;9pm and the days are in the first part of this string
        // so we can split by ; and then just take the first element
        return DateTimeUtil.createDays(value.split(";").first().split("-").map { dayOfWeek(it) })
    }
}