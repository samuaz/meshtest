import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit

class RecommendationService(private val extractor: Extractor) {

    fun findAvailableService(
        stores: List<CsvRow>,
        localDateTIme: LocalDateTime = LocalDateTime.now()
    ): List<StoreTodaySchedule> {
        return execute(stores).map { store ->
            store.dayTimes.mapNotNull {
                if (localDateTIme.toLocalTime() in it.times && localDateTIme.dayOfWeek == it.dayOfWeek) {
                    StoreTodaySchedule(store.name, store.categories, it.dayOfWeek, it.times)
                } else {
                    null
                }
            }
        }.flatten()
    }

    fun findAvailableServiceByCategories(
        stores: List<CsvRow>,
        localDateTIme: LocalDateTime = LocalDateTime.now(),
        categories: List<String>
    ): List<StoreTodaySchedule> {
        val foundStores = mutableListOf<StoreTodaySchedule>()
        findAvailableService(stores, localDateTIme).forEach { storeTodaySchedule ->
            categories.forEach {
                if (it in storeTodaySchedule.categories) {
                    foundStores.add(storeTodaySchedule)
                }
            }
        }
        return foundStores
    }

    private fun execute(csvRows: List<CsvRow>): List<Store> {
        return csvRows.map { place ->
            // As we have the schedules of each range of days separated by / (Mon-Thu;11:30am;9pm / Sun;11:30am;9pm / Fri-Sat;11:30am;9:30pm) we need to separate and extract it
            val times = place.openCloseTime.replace("\\s".toRegex(), "").split("/")
            // now we have a list of the ranges example: ["Mon-Thu;11:30am;9pm","Sun;11:30am;9pm","Fri-Sat;11:30am;9:30pm"]
            times.map { days ->
                // now we need to extract the time periods
                val timeRange = extractor.extractTimes(days)
                // lets extract the days we know that the days come in format start-end Mon-Thu
                extractor.extractDays(days).map {
                    // as the time for a day can come in unusual format like saturday 3pm to 1:30am this mean that start on saturday but takes some hours from the next day sunday
                    // so we need to check that to know if the difference between the two times is a negative value
                    val hourDifference = ChronoUnit.HOURS.between(timeRange[0], timeRange[1])
                    // but also is possible that comes only minutes after the midnight like saturday 3pm to 0:05am so we need to check that also
                    // so we say if the hour differences are < 0 and the minutes are > 0 OR there is not hour difference but we have a minutes differences
                    if ((hourDifference < 0 && timeRange[1].minute > 0) || (hourDifference == 0L && timeRange[1].minute > 0)) {
                        // so if this is true we say that for example the end day of the saturday is midnight but we add a new day where the start is midnight and the end the remaining hours between previous day and midnight
                        listOf(
                            DayTime(it, timeRange[0]..LocalTime.MIDNIGHT),
                            DayTime(it.plus(1), LocalTime.MIDNIGHT..timeRange[1])
                        )
                    } else {
                        listOf(DayTime(it, timeRange[0]..timeRange[1]))
                    }
                }.flatten()
            }.flatten().let { dayTimes ->
                Store(place.name, place.category.split("/"), dayTimes.sortedBy { it.dayOfWeek })
            }
        }
    }
}