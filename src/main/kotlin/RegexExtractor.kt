import util.DateTimeUtil
import java.time.DayOfWeek
import java.time.LocalTime

object RegexExtractor: Extractor {
    private val daysRegex = "\\b((?i)(mon|tue|tues|wed(nes)?|thu|thur(s)?|fri|sat(ur)?|sun)(day)?)\\b".toRegex()
    private val timeRegex = "\\d{1,2}(am|pm)|\\d{1,2}:\\d{2}(am|pm)".toRegex()

    override fun extractTimes(value: String): List<LocalTime> {
        return timeRegex.findAll(value).map { DateTimeUtil.formatTime(it.value) }.asIterable().toList()
    }

    override fun extractDays(value: String): List<DayOfWeek> {
        return DateTimeUtil.createDays(daysRegex.findAll(value).map { DateTimeUtil.dayOfWeek(it.value)}.asIterable().toList()).asIterable().toList()

    }
}