import java.time.DayOfWeek
import java.time.LocalTime

interface Extractor {
    fun extractDays(value: String): List<DayOfWeek>
    fun extractTimes(value: String): List<LocalTime>
}