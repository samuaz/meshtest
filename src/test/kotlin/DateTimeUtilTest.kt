import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import util.DateTimeUtil
import java.time.DayOfWeek
import java.time.LocalTime

class DateTimeUtilTest {
    @Test
    fun testCanParseDays() {
        assertThat(DateTimeUtil.dayOfWeek("mon")).isEqualTo(DayOfWeek.MONDAY)
        assertThat(DateTimeUtil.dayOfWeek("tue")).isEqualTo(DayOfWeek.TUESDAY)
        assertThat(DateTimeUtil.dayOfWeek("wed")).isEqualTo(DayOfWeek.WEDNESDAY)
        assertThat(DateTimeUtil.dayOfWeek("thu")).isEqualTo(DayOfWeek.THURSDAY)
        assertThat(DateTimeUtil.dayOfWeek("fri")).isEqualTo(DayOfWeek.FRIDAY)
        assertThat(DateTimeUtil.dayOfWeek("sat")).isEqualTo(DayOfWeek.SATURDAY)
        assertThat(DateTimeUtil.dayOfWeek("sun")).isEqualTo(DayOfWeek.SUNDAY)
    }

    @Test
    fun testCanParseTime() {
        assertThat(DateTimeUtil.formatTime("9am")).isEqualTo(LocalTime.of(9,0))
        assertThat(DateTimeUtil.formatTime("9:30pm")).isEqualTo(LocalTime.of(21,30))
        assertThat(DateTimeUtil.formatTime("1am")).isEqualTo(LocalTime.of(1,0))
    }

    @Test
    fun canCreateDays() {
        assertThat(DateTimeUtil.createDays(listOf(DayOfWeek.MONDAY, DayOfWeek.SUNDAY))).containsAll(
            listOf(DayOfWeek.MONDAY,DayOfWeek.TUESDAY,DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY)
        )
    }
}