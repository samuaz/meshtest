import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.4.32"
    application
}

group = "me.samuaz"
version = "1.0-SNAPSHOT"
val ktorVersion = "1.5.0"
val slf4jVersion = "1.7.30"
val exposedVersion = "0.29.1"
val koinVersion = "2.1.6"
val oktaJwtVersion = "0.5.0"
val testContainersVersion = "1.15.1"
val resilience4jVersion = "1.5.0"
val strataVersion = "2.8.10"
repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/kotlin/exposed")
    }
}

dependencies {
    testImplementation(kotlin("test-junit"))
    testImplementation("junit", "junit", "4.13.1")
    testImplementation("org.assertj", "assertj-core", "3.17.2")
    implementation("org.jetbrains.kotlinx:kotlinx-cli:0.3.2")
    implementation("org.jetbrains.exposed", "exposed-core", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-java-time", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)
    implementation("org.jetbrains.kotlin", "kotlin-reflect")
    implementation("org.jetbrains.kotlin", "kotlin-stdlib")
    implementation("org.jetbrains.kotlin", "kotlin-stdlib-jdk8")
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

application {
    mainClass.set("MainKt")
}